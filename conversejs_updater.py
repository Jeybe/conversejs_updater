import requests
from bs4 import BeautifulSoup
import re
import tarfile
import shutil
import os

# ---- Configuration ----

conversejs_install_path="/var/www/conversejs/dist"
conversejs_major_release="8"

# ---- End of configuration ----

conversejs_code_hoster = "https://github.com"
conversejs_release_page = "https://github.com/conversejs/converse.js/releases/latest"
conversejs_release_partial_filename="converse.js-" + conversejs_major_release;

def extract_links(url):
        reqs = requests.get(url)
        soup = BeautifulSoup(reqs.text, 'html.parser')
        
        urls = []
        for link in soup.find_all('a'):
            urls.append(link.get('href'))
        return urls

def find_conversejs_release_path():
    release_page_links = extract_links(conversejs_release_page)
    for link in release_page_links:
        if link.find(conversejs_release_partial_filename) != -1:
            return link
    return None

def find_conversejs_release_url():
    conversejs_release_url = conversejs_code_hoster + find_conversejs_release_path()
    return conversejs_release_url

def install_conversejs_release():
    # download tarball
    url = find_conversejs_release_url()
    req = requests.get(url)
    filename = url.split('/')[-1]
    
    with open(filename, 'wb') as output_file:
        output_file.write(req.content)
    # extract tarball
    tarball = tarfile.open(filename)
    tarball.extractall()
    tarball.close()
    # move files
    os.remove(filename)
    shutil.move('package/dist',conversejs_install_path)
    shutil.rmtree('package')
    

if __name__ == "__main__":
    install_conversejs_release()
