# ConverseJS updater

This is a little Python script I wrote to update an ConverseJS installation.

## What does this script do

It determines the latest v7 release of ConverseJS, downloads it and moves it to the specified path.

## Configuration

At the top of the script there is a commented configuration section. The variables within that section are meant to be edited to fit your need. Below is a short explanation of what the specific configuration variables are good for.

### `conversejs_install_path`

This is the path to the directory where your Element Web installation is located. By default this is set to `/var/www/conversejs`.

## Installing ConverseJS for the first time with this script

Altough the main purpose of this script is to update an existing ConverseJS instance, it can also be used to streamline the process of installing ConverseJS. To do so, just adjust the configure options to your likes, run it and follow the [installation docs from ConverseJS](https://conversejs.org/docs/html/index.html) to configure it to your needs.

## Licence

All files within this repository are licenced under the [MIT Licence](LICENCE).

